//
//  ViewController.m
//  TicketSim
//
//  Created by Brice Wilson on 2/6/14.
//  Copyright (c) 2014 Brice Wilson. All rights reserved.
//

#import "ViewController.h"
#import "Simulator.h"

@interface ViewController () {
    
    NSArray *customers;
    int currentCustomerIndex;
    NSOperationQueue *ticketQueue;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    customers = [NSArray arrayWithObjects:@"Audrey", @"Bub", @"Cynthia", @"Darshan", @"Evan", nil];
    currentCustomerIndex = 0;
    [[self customerNameLabel] setText:[customers objectAtIndex:currentCustomerIndex]];
    
    // initialize operation queue
    ticketQueue = [[NSOperationQueue alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buyTicketsClicked:(id)sender {
    
    NSString *currentCustomerName = [customers objectAtIndex:currentCustomerIndex];

    //**************************************************************

    // run simulations on UI thread - example of what NOT to do
    double time = [Simulator runSimulationWithMinTime:2 maxTime:5];
    [self logResult:[NSString stringWithFormat:@"%@ bought tickets in %.02f seconds.", currentCustomerName, time]];

    //**************************************************************

    // run simulation on an operation queue - no NSOperation declaration
//    [ticketQueue addOperationWithBlock:^{
//        double time = [Simulator runSimulationWithMinTime:2 maxTime:5];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self logResult:[NSString stringWithFormat:@"%@ bought tickets in %.02f seconds.", currentCustomerName, time]];
//        });
//    }];
    
    //**************************************************************
    
    // declare and initialize an NSOperation variable
//    NSBlockOperation *buyTicketsOp = [NSBlockOperation blockOperationWithBlock:^{
//        double time = [Simulator runSimulationWithMinTime:2 maxTime:5];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self logResult:[NSString stringWithFormat:@"%@ bought tickets in %.02f seconds.", currentCustomerName, time]];
//        });
//    }];
//
//    // add completion block to operation
//    [buyTicketsOp setCompletionBlock:^{
//        NSLog(@"Buy tickets operation completed for %@", currentCustomerName);
//    }];
//    
//    // add the NSOperation to the queue
//    [ticketQueue addOperation:buyTicketsOp];

    //**************************************************************

    // declare and initialize an NSOperation variable
//    NSBlockOperation *buyTicketsOp = [NSBlockOperation blockOperationWithBlock:^{
//        double time = [Simulator runSimulationWithMinTime:2 maxTime:5];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self logResult:[NSString stringWithFormat:@"%@ bought tickets in %.02f seconds.", currentCustomerName, time]];
//        });
//    }];
//
//    // add completion block to operation
//    [buyTicketsOp setCompletionBlock:^{
//        NSLog(@"Buy tickets operation completed for %@", currentCustomerName);
//    }];
//
//    // create a new NSOperation to handle payment
//    NSBlockOperation *payOp = [NSBlockOperation blockOperationWithBlock:^{
//        double time = [Simulator runSimulationWithMinTime:4 maxTime:10];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self logResult:[NSString stringWithFormat:@"%@ paid in %.02f seconds.", currentCustomerName, time]];
//        });
//    }];
//
//    // make payment dependent on tickets already being bought
//    [payOp addDependency:buyTicketsOp];
//
//    // add the payOp operation to the queue first
//    // just to show the dependency will keep it from running first
//    [ticketQueue addOperation:payOp];
//
//    // add the buyTicketsOp to the queue
//    [ticketQueue addOperation:buyTicketsOp];

    //**************************************************************
    
    if (currentCustomerIndex == [customers count] - 1) {
        [[self customerNameLabel] setText:@"No more customers"];
        [[self buyTicketsButton] setEnabled:NO];
    } else {
        [[self customerNameLabel] setText:[customers objectAtIndex:++currentCustomerIndex]];
    }
}

- (IBAction)cancelClicked:(id)sender {
    [ticketQueue cancelAllOperations];
}



- (IBAction)resetClicked:(id)sender {
    [[self outputTextView] setText:@""];
    currentCustomerIndex = 0;
    [[self customerNameLabel] setText:[customers objectAtIndex:currentCustomerIndex]];
    [[self buyTicketsButton] setEnabled:YES];
}

- (IBAction)alphaChanged:(id)sender {
    UIColor *currentColor = [[self view] backgroundColor];
    [[self view] setBackgroundColor:[currentColor colorWithAlphaComponent:[[self alphaSlider] value]]];
}

- (void)logResult:(NSString *)message {
    
    NSString *contents = [[NSString alloc] init];
    
    if ([[self outputTextView] hasText]) {
        contents = [[[self outputTextView] text] stringByAppendingString:@"\n"];
    }
    
	contents = [contents stringByAppendingString:message];
	[[self outputTextView] setText:contents];
    
}

@end
